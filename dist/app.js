"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var biscuit_server_1 = require("./biscuit-server");
var app = new biscuit_server_1.BiscuitServer().getApp();
exports.app = app;
