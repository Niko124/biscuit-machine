import { BiscuitMachine } from './biscuit-machine';
import { Status, IState } from './types';
import { SocketClient } from './socket/socket-types';

export class BMVersion1 extends BiscuitMachine {
  constructor(state: IState, socket: SocketClient) {
    super(state, socket);
  }

  public ovenOn(): void {
    this.oven.updateStatus(Status.ON);
  }
  public ovenOff(): void {
    this.oven.updateStatus(Status.OFF);
  }
  public motorOn(): void {
    this.motor.updateStatus(Status.ON);
  }
  public motorOff(): void {
    this.motor.updateStatus(Status.OFF);
  }
}
