import { BiscuitMachine } from './biscuit-machine';

export interface IState {
  on(bm: BiscuitMachine): void;
  off(bm: BiscuitMachine): void;
  pause(bm: BiscuitMachine): void;
}

export const enum Status {
  ON = 'on',
  OFF = 'off',
  PAUSE = 'pause',
}

export interface MinMaxOvenTemp {
  min: number;
  max: number;
}

export interface MotorData {
  extruder: string;
  stamper: string;
  conveyer: string[];
  bakedContainer: string[];
}

export const enum DevicesesStatus {
  ACTIVE = 'ACTIVE',
  NOTACTIVE = 'NOT-ACTIVE',
}

export const enum Devices {
  Extruder,
  Stamper,
  Container,
}
