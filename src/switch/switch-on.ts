import { BiscuitMachine } from '../biscuit-machine';
import { SwitchOFF } from './switch-off';
import { SwitchPAUSE } from './switch-pause';
import { IState } from '../types';

export class SwitchON implements IState {
  on(bm: BiscuitMachine): void {
    console.log('I AM ALREADY ON');
  }
  off(bm: BiscuitMachine): void {
    bm.setState(new SwitchOFF());
    bm.ovenOff();
    bm.motorOff();
  }
  pause(bm: BiscuitMachine): void {
    bm.setState(new SwitchPAUSE());
    bm.motorOff();
  }
}
