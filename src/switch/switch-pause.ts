import { BiscuitMachine } from '../biscuit-machine';
import { SwitchON } from './switch-on';
import { SwitchOFF } from './switch-off';
import { IState } from '../types';

export class SwitchPAUSE implements IState {
  on(bm: BiscuitMachine): void {
    bm.setState(new SwitchON());
    bm.ovenOn();
    bm.motorOn();
  }
  off(bm: BiscuitMachine): void {
    bm.setState(new SwitchOFF());
    bm.ovenOff();
    bm.motorOff();
  }
  pause(bm: BiscuitMachine): void {
    console.log('ALREADY PAUSED');
  }
}
