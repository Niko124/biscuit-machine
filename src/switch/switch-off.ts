import { BiscuitMachine } from '../biscuit-machine';
import { SwitchON } from './switch-on';
import { IState } from '../types';

export class SwitchOFF implements IState {
  private tempInterval!: NodeJS.Timer | null;

  on(bm: BiscuitMachine): void {
    bm.setState(new SwitchON());
    bm.ovenOn();
    this.runMotor(bm);
  }
  off(bm: BiscuitMachine): void {
    console.log('I AM ALREADY OFF');
  }
  pause(bm: BiscuitMachine): void {
    console.log('I CANT SWITCH PAUSE');
  }

  private runMotor(bm: BiscuitMachine): void {
    if (!this.tempInterval) {
      this.tempInterval = setInterval(() => {
        const ovenTemps = bm.oven.getOvenTemps();

        if (ovenTemps.ovenTemp >= ovenTemps.minCookTemp) {
          bm.motorOn();
          clearInterval(this.tempInterval as NodeJS.Timer);
          this.tempInterval = null;
        }
      }, 100);
    }
  }
}
