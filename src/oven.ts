import { Status } from './types';
import { SocketClient } from './socket/socket-types';

export class Oven {
  private roomTemp: number = 30;
  private ovenTemp: number = this.roomTemp;
  private minCookTemp: number = 220;
  private maxCookTemp: number = 240;
  private heatTemp: number = 10;
  private coolDownTemp: number = 5;
  private heatUpInterval!: NodeJS.Timer | null;
  private coolDownInterval!: NodeJS.Timer | null;
  private currentStatus!: Status;

  private socket!: SocketClient;

  constructor(socket: SocketClient) {
    this.socket = socket;
    this.socket.on('oven', (_: Status) => {
      console.log('OVEN connection is open');
    });
  }

  public updateStatus(status: Status): void {
    this.currentStatus = status;
    if (status === Status.ON) {
      this.heatUp();
    }
  }

  public getOvenTemps(): { ovenTemp: number; minCookTemp: number } {
    return {
      ovenTemp: this.ovenTemp,
      minCookTemp: this.minCookTemp,
    };
  }

  private heatUp(): void {
    if (!this.heatUpInterval) {
      this.heatUpInterval = setInterval(() => {
        this.socket.emit('oven', this.ovenTemp);
        if (this.ovenTemp >= this.maxCookTemp) {
          clearInterval(this.heatUpInterval as NodeJS.Timer);
          this.heatUpInterval = null;
          this.coolDown();
          return;
        }
        this.ovenTemp = this.ovenTemp + this.heatTemp;
      }, 500);
    }
  }

  private coolDown(): void {
    if (!this.coolDownInterval) {
      this.coolDownInterval = setInterval(() => {
        this.socket.emit('oven', this.ovenTemp);
        if (this.currentStatus === Status.ON) {
          if (this.ovenTemp <= this.minCookTemp) {
            clearInterval(this.coolDownInterval as NodeJS.Timer);
            this.coolDownInterval = null;
            this.heatUp();
            return;
          }
        } else if (this.currentStatus === Status.OFF) {
          if (this.ovenTemp === this.roomTemp) {
            clearInterval(this.coolDownInterval as NodeJS.Timer);
            this.coolDownInterval = null;
            return;
          }
        }

        this.ovenTemp = this.ovenTemp - this.coolDownTemp;
      }, 2000);
    }
  }
}
