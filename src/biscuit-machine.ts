import { Oven } from './oven';
import { Motor } from './motor';
import { SocketClient } from './socket/socket-types';
import { IState } from './types';

export abstract class BiscuitMachine {
  public oven!: Oven;
  public motor!: Motor;

  private state!: IState;
  private socket!: SocketClient;

  constructor(state: IState, socket: SocketClient) {
    this.socket = socket;
    this.oven = new Oven(this.socket);
    this.motor = new Motor(this.socket);
    this.setState(state);
  }

  public setState(state: IState): void {
    console.log(`Context: Transition to ${state.constructor.name}.`);
    this.state = state;
  }

  public on() {
    this.state.on(this);
  }

  public off() {
    this.state.off(this);
  }

  public pause() {
    this.state.pause(this);
  }

  public abstract ovenOn(): void;
  public abstract ovenOff(): void;
  public abstract motorOn(): void;
  public abstract motorOff(): void;
}
