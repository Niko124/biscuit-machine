export interface WebSocketConfiguration {
  server: any;
  opts?: {};
}
export interface SocketFactory {
  createSocket(configuration: WebSocketConfiguration): any;
}
export interface SocketClient {
  connect(configuration: WebSocketConfiguration): Promise<any>;
  close(): Promise<any>;
  emit(event: string, ...args: any[]): Promise<any>;
  on(event: string, fn: SocketCallbackFn): Promise<any>;
}

export type SocketCallbackFn = (data: any) => any | void;
