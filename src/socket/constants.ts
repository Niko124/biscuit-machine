const webSocketFactoryType: symbol = Symbol('WebSocketFactory');
const webSocketClientType: symbol = Symbol('WebSocketClient');

export const TYPES: any = {
  WebSocketFactory: webSocketFactoryType,
  WebSocketClient: webSocketClientType,
};
