import socketIo from 'socket.io';
import { SocketFactory, WebSocketConfiguration } from './socket-types';
import { injectable } from 'inversify';

@injectable()
export class SocketIOFactory implements SocketFactory {
  createSocket(configuration: WebSocketConfiguration): SocketIO.Server {
    return socketIo(configuration.server, configuration.opts);
  }
}
