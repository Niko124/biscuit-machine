import { Container } from 'inversify';
import 'reflect-metadata';
import { SocketClient, SocketFactory } from './socket-types';
import { TYPES } from './constants';
import { WebSocketClient } from './websocket-client';
import { SocketIOFactory } from './socket-factory';

const provider = new Container({ defaultScope: 'Singleton' });

provider.bind<SocketClient>(TYPES.WebSocketClient).to(WebSocketClient);
provider.bind<SocketFactory>(TYPES.WebSocketFactory).to(SocketIOFactory);

export default provider;
