import { injectable, inject } from 'inversify';
import {
  SocketClient,
  SocketFactory,
  WebSocketConfiguration,
  SocketCallbackFn,
} from './socket-types';
import { TYPES } from './constants';

@injectable()
export class WebSocketClient implements SocketClient {
  private socketFactory: SocketFactory;
  private socket: any;

  public constructor(
    @inject(TYPES.WebSocketFactory) webSocketFactory: SocketFactory,
  ) {
    this.socketFactory = webSocketFactory;
  }

  public connect(config: WebSocketConfiguration): Promise<any> {
    if (!this.socket) {
      this.socket = this.socketFactory.createSocket(config);
    }

    return new Promise<any>((resolve, reject) => {
      this.socket.on('connect', () => resolve());
      this.socket.on('connect_error', (error: Error) => reject(error));
    });
  }

  public emit(event: string, ...args: any[]): Promise<any> {
    return new Promise<string | {}>((resolve, reject) => {
      if (!this.socket) {
        return reject('No socket connection.');
      }

      return this.socket.emit(event, args, (response: any) => {
        if (response.error) {
          return reject(response.error);
        }

        return resolve();
      });
    });
  }

  public on(event: string, fn: SocketCallbackFn): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (!this.socket) {
        return reject('No socket connection.');
      }

      this.socket.on(event, fn);
      resolve();
    });
  }

  public close(): Promise<any> {
    return new Promise<any>((resolve) => {
      this.socket.close(() => {
        this.socket = null;
        resolve();
      });
    });
  }
}
