import { Devices, DevicesesStatus, MotorData } from './types';

export const revolutionMotor = async (actions: Promise<boolean>[]) => {
  for (const action of actions) {
    await action;
  }
};

export const generateAction = (
  device: Devices,
  conveyer: string[],
  bakedContainer: string[],
): MotorData => {
  if (device === Devices.Extruder) {
    return {
      conveyer,
      extruder: DevicesesStatus.ACTIVE,
      stamper: DevicesesStatus.NOTACTIVE,
      bakedContainer,
    };
  } else if (device === Devices.Stamper) {
    return {
      conveyer,
      extruder: DevicesesStatus.NOTACTIVE,
      stamper: DevicesesStatus.ACTIVE,
      bakedContainer,
    };
  } else {
    return {
      conveyer,
      extruder: DevicesesStatus.NOTACTIVE,
      stamper: DevicesesStatus.NOTACTIVE,
      bakedContainer,
    };
  }
};
