import { Status, Devices } from './types';
import { SocketClient } from './socket/socket-types';
import { revolutionMotor, generateAction } from './utils';

export class Motor {
  private conveyer: string[] = [];
  private revolutionInterval!: NodeJS.Timer | null;
  private clearConveyerInterval!: NodeJS.Timer | null;
  private biscuitInterval!: NodeJS.Timer | null;
  private socket!: SocketClient;
  private bakedContainer: string[] = [];

  constructor(socket: SocketClient) {
    this.socket = socket;
    this.socket.on('motor', (_: Status) => {
      console.log('MOTOR connection is open');
    });
  }
  public updateStatus(status: Status): void {
    if (status === Status.ON) {
      this.start();
    } else if (status === Status.OFF) {
      this.stop();
    } else {
      this.pause();
    }
  }

  private start(): void {
    this.revolution();
    this.clearConveyer();
  }

  private stop(): void {
    clearInterval(this.revolutionInterval as NodeJS.Timer);
    clearInterval(this.biscuitInterval as NodeJS.Timer);
    this.revolutionInterval = null;
    this.biscuitInterval = null;
  }

  private pause(): void {
    clearInterval(this.revolutionInterval as NodeJS.Timer);
    clearInterval(this.biscuitInterval as NodeJS.Timer);
    clearInterval(this.clearConveyerInterval as NodeJS.Timer);
    this.revolutionInterval = null;
    this.biscuitInterval = null;
    this.clearConveyerInterval = null;
  }

  private extruder(): Promise<boolean> {
    return new Promise((resolve) => {
      setTimeout(() => {
        this.socket.emit(
          'motor',
          generateAction(Devices.Extruder, this.conveyer, this.bakedContainer),
        );
        resolve(true);
      }, 1000);
    });
  }

  private stamper(): Promise<boolean> {
    return new Promise((resolve) => {
      setTimeout(() => {
        this.socket.emit(
          'motor',
          generateAction(Devices.Stamper, this.conveyer, this.bakedContainer),
        );
        resolve(true);
      }, 2000);
    });
  }

  private async revolution() {
    if (!this.revolutionInterval) {
      this.revolutionInterval = setInterval(() => {
        revolutionMotor([this.extruder(), this.stamper()]).then(() => {
          this.addBiscuit();
        });
      }, 5000);
    }
  }

  private clearConveyer(): void {
    if (!this.clearConveyerInterval) {
      this.clearConveyerInterval = setInterval(() => {
        if (this.conveyer.length) {
          this.cleaning();
        }
      }, 20000);
    }
  }

  private cleaning(): void {
    const biscuit = this.conveyer.shift();
    this.bakedContainer.push(biscuit as string);
    this.socket.emit(
      'motor',
      generateAction(Devices.Container, this.conveyer, this.bakedContainer),
    );
  }

  private addBiscuit(): void {
    this.conveyer.push('biscuit');
    setTimeout(() => {
      this.socket.emit(
        'motor',
        generateAction(Devices.Container, this.conveyer, this.bakedContainer),
      );
    }, 1000);
  }
}
