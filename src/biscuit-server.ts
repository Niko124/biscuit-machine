import { createServer, Server } from 'http';
import express from 'express';
import socketIo from 'socket.io';
import { Status } from './types';
import { BMVersion1 } from './biscuit-machine-v1';
import { SwitchOFF } from './switch/switch-off';
import { SocketClient } from './socket/socket-types';
import { TYPES } from './socket/constants';
import provider from './socket/socket-container';

export class BiscuitServer {
  public static readonly PORT: number = 8080;
  private app!: express.Application;
  private server!: Server;
  private io!: any;
  private port!: string | number;
  private BMVersion1!: BMVersion1;

  constructor() {
    this.createApp();
    this.config();
    this.createServer();
    this.sockets();
    this.listen();
  }

  private createApp(): void {
    this.app = express();
  }

  private createServer(): void {
    this.server = createServer(this.app);
  }

  private config(): void {
    this.port = process.env.PORT || BiscuitServer.PORT;
  }

  private sockets(): void {
    this.io = provider.get<SocketClient>(TYPES.WebSocketClient);
    this.io.connect({ server: this.server });
  }

  private listen(): void {
    this.server.listen(this.port, () => {
      console.log('Running server on port %s', this.port);
    });

    this.io.on('connect', (socket: SocketClient) => {
      this.BMVersion1 = new BMVersion1(new SwitchOFF(), socket);
      console.log('Connected client on port %s.', this.port);

      socket.on('switch', (data: Status) => {
        if (data === Status.ON) {
          this.BMVersion1.on();
        } else if (data === Status.OFF) {
          this.BMVersion1.off();
        } else {
          this.BMVersion1.pause();
        }
      });

      socket.on('disconnect', () => {
        console.log('Client disconnected');
      });
    });
  }

  public getApp(): express.Application {
    return this.app;
  }
}
