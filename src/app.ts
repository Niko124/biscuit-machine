import { BiscuitServer } from './biscuit-server';

const app = new BiscuitServer();

export { app };
